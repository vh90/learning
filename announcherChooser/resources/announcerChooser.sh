#!/bin/bash

readarray persons < announcers.txt

echo "Candidates:"
for i in "${persons[@]}"; do
	echo -e '\t' $i
done

echo -e "\nAnd the winner is... " ${persons["$[RANDOM % ${#persons[@]}]"]}

