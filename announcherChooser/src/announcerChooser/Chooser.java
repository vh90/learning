package announcerChooser;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

public class Chooser {

	public static void main(String[] args) throws Exception {
		
		if (args.length != 1) {
			System.out.println("Usage: java -jar chooser.jar studentsFile");
			System.out.println("e.g.   java -jar chooser.jar /Data/Reporting/vacca/Studio/001-class_collaborationDiagrams/announcers.txt");
			return;
		}

		File excludedFile = new File("log/" + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()));

		List<String> students = FileUtils.readLines(new File(args[0]));
		print("Students", students);

		Scanner scanner = new Scanner(System.in);

		String input = "";
		do {
			input = scanner.nextLine();
			if (students.remove(input)) {
				FileUtils.write(
						excludedFile,
						excludedFile.exists() ? System.lineSeparator() + input : input,
						true
					);
			}
		} while (StringUtils.isNotBlank(input));

		scanner.close();

		print("Candidates", students);

		int element = new Random().nextInt(students.size());
		System.out.println("And the winner is... " + students.get(element));
	}

	private static void print(String title, List<String> announcers) {
		System.out.println(title);
		for (String string : announcers) {
			System.out.println("\t"+string);
		}
	}
}
