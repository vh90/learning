package smell;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class OrderMapperTest {

	@Test
	public void testSaveOrder() throws Exception {
		FakeConnection connection = new FakeConnection();

		new OrderMapper(connection).saveOrder();

		assertThat(connection.status().size(), is(1));
		assertTrue(connection.status().contains("INSERT INTO T_ORDER (AUTHORIZATION_CODE, SHIPMETHOD_ID, USER_ID, ADDRESS_ID) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"));
	}

}
