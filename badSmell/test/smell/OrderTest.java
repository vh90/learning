package smell;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.junit.Before;
import org.junit.Test;

public class OrderTest {

	private Order _order;

	@Before
	public void setUp() {
		LineItem lineItem1 = new LineItem(0, 0, 2, 10);
		LineItem lineItem2 = new LineItem(1, 1, 3, 20);

		_order = new Order(lineItem1, lineItem2);
	}

	@Test
	public void testWriteOrder() {
		
		StringWriter result = new StringWriter();
		_order.writeOrder(new PrintWriter(result));

		String expected = 
			"Begin Line Item" + System.lineSeparator() +
            "Product = 0" + System.lineSeparator() +
            "Image = 0" + System.lineSeparator() +
            "Quantity = 2" + System.lineSeparator() +
            "Total = 20" + System.lineSeparator() +
            "End Line Item" + System.lineSeparator() +
            "Begin Line Item" + System.lineSeparator() +
            "Product = 1" + System.lineSeparator() +
            "Image = 1" + System.lineSeparator() +
            "Quantity = 3" + System.lineSeparator() +
            "Total = 60" + System.lineSeparator() +
            "End Line Item" + System.lineSeparator() +
            "Order total = 80" + System.lineSeparator();
		assertThat(result.toString(), is(expected));
	}

	@Test
	public void testGetTotal() {
		assertThat(_order.getTotal(), is(80));
	}
}
