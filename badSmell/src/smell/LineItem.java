package smell;

import java.io.PrintWriter;

class LineItem {

	private final int _productId;
	private final int _imageID;
	private final int _qty;
	private final int _unitPrice;

	public LineItem(int prodID, int ImageID, int inQty, int unitPrice) {
		_productId = prodID;
		_imageID = ImageID;
		_qty = inQty;
		_unitPrice = unitPrice;
	}

	int total() {
		return _unitPrice * _qty;
	}

	void print(PrintWriter pw) {
		pw.println("Begin Line Item");
		pw.println("Product = " + _productId);
		pw.println("Image = " + _imageID);
		pw.println("Quantity = " + _qty);
		pw.println("Total = " + total());
		pw.println("End Line Item");
	}
}