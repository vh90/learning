package smell;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class OrderMapper {

	private final Connection _connection;

	public OrderMapper(Connection connection) {
		_connection = connection;
	}
	public void saveOrder() throws SQLException {

		String sql = "INSERT INTO T_ORDER " +
				"(AUTHORIZATION_CODE, " +
				"SHIPMETHOD_ID, USER_ID, ADDRESS_ID) " +
				"VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		PreparedStatement orderStatement = _connection.prepareStatement(sql);
		//set all parameters

		//execute statement
		orderStatement.executeUpdate();
	}
}
