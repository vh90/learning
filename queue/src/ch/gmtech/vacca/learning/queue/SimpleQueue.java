package ch.gmtech.vacca.learning.queue;

public class SimpleQueue implements Queue {

	private Integer _front = 0;
	private Integer _rear = -1;

	private final Integer[] _queue = new Integer[1000];

	public boolean isEmpty() {
		return _front > _rear;
	}

	public Integer peek() {

		if (isEmpty()) {
			return null;
		}

		return _queue[_front];
	}

	// essendo illimitata, non necessita di uno stato di ritorno 
	@Override
	public void enqueue(Integer data) {

		_rear = _rear + 1;
		_queue[_rear] = data;

	}

	@Override
	public Integer dequeue() {

		if (isEmpty()) {
			return null;
		}

		Integer data = _queue[_front];
		_front = _front + 1;

		return data;
	}
}
