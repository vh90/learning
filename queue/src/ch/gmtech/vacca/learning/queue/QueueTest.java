package ch.gmtech.vacca.learning.queue;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class QueueTest {

	private SimpleQueue _queue;

	@Before
	public void setUp() {
		_queue = new SimpleQueue();
	}

	@Test
	public void testEmpty() {
		assertNull(_queue.peek());
		assertNull(_queue.dequeue());
	}

	@Test
	public void testSingle() {
		_queue.enqueue(1);

		assertEquals(new Integer(1), _queue.peek());
		assertFalse(_queue.isEmpty());
		assertEquals(new Integer(1), _queue.dequeue());
		assertTrue(_queue.isEmpty());
	}
		
	@Test
	public void testManyElements() {
		_queue.enqueue(10);
		_queue.enqueue(11);

		assertEquals(new Integer(10), _queue.peek());
		assertEquals(new Integer(10), _queue.dequeue());
		assertFalse(_queue.isEmpty());
	}
}
