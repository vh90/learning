package ch.gmtech.vacca.learning.queue;

public class MonitorableQueue implements Queue {

	private Integer _maxElements = 0;
	private Integer _currentElements = 0;

	private final Queue _queue = new SimpleQueue();

	@Override
	public void enqueue(Integer data) {
		_currentElements++;

		if (_currentElements > _maxElements) {
			_maxElements = _currentElements;
		}

		_queue.enqueue(data);
	}

	@Override
	public Integer dequeue() {
		if (! isEmpty()) {
			_currentElements--;
		}

		return _queue.dequeue();
	}

	public Integer maxElements() {
		return _maxElements;
	}

	@Override
	public boolean isEmpty() {
		return _queue.isEmpty();
	}
}
