package ch.gmtech.vacca.learning.queue;

public interface Queue {

	void enqueue(Integer data);
	Integer dequeue();
	boolean isEmpty();
}
