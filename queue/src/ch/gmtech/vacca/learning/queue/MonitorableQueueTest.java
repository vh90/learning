package ch.gmtech.vacca.learning.queue;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MonitorableQueueTest {

	MonitorableQueue _queue;
	@Before
	public void setUp() {
		_queue = new MonitorableQueue();
	}

	@Test
	public void test() {
		_queue.enqueue(0);
		_queue.enqueue(0);
		_queue.enqueue(0);
		assertEquals(new Integer(3), _queue.maxElements());

		_queue.dequeue();
		assertEquals(new Integer(3), _queue.maxElements());

		_queue.enqueue(0);
		_queue.enqueue(0);
		assertEquals(new Integer(4), _queue.maxElements());
	}

	@Test
	public void testEmpty() {
		_queue.dequeue();
		_queue.dequeue();
		_queue.dequeue();
		_queue.enqueue(0);
		assertEquals(new Integer(1), _queue.maxElements());
	}
}
