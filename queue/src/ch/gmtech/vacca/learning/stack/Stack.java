package ch.gmtech.vacca.learning.stack;

public interface Stack {

	void push(Object o);
	Object pop();
	void push_many(Object[] source);
}
