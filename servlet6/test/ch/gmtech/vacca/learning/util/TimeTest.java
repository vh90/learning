package ch.gmtech.vacca.learning.util;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void render() {
		Time time = new Time("01.01.1970");
		assertThat(time.toString(), is("01.01.1970"));
	}

	@Test(expected=RuntimeException.class)
	public void unsupportedPattern() {
		new Time("1970-01-01");
	}
}
