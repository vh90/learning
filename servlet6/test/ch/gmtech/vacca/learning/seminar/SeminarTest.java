package ch.gmtech.vacca.learning.seminar;

import static org.junit.Assert.*;

import org.junit.Test;

import ch.gmtech.vacca.learning.util.Time;

public class SeminarTest {

	@Test
	public void testStudents() {
		Course course = new Course(0, "courseName", "description", "Lugano", new Time("01.09.2016"), 20);
		assertEquals(0, course.students().size());

		course.enroll(new Student("Pippo", "Pluto"));
		assertEquals(1, course.students().size());
	}

	@Test(expected=UnsupportedOperationException.class)
	public void preventManualIntervention() {
		Course course = new Course(0, "courseName", "description", "Lugano", new Time("01.09.2016"), 20);
		course.students().clear();
	}

	@Test
	public void dontEnrollTooMuchStudents() {
		Course course = new Course(0, "courseName", "description", "Lugano", new Time("01.09.2016"), 1);

		assertTrue(course.enroll(new Student("Ugo", "Campione")));
		assertFalse(course.enroll(new Student("Laggard", "Guy")));
	}
}
