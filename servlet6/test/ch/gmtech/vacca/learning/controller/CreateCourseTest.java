package ch.gmtech.vacca.learning.controller;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;

import ch.gmtech.vacca.learning.FakeHttpServletRequest;
import ch.gmtech.vacca.learning.FakeHttpServletResponse;
import ch.gmtech.vacca.learning.seminar.Course;

public class CreateCourseTest {

	@Test
	public void form() throws Exception {
		FakeHttpServletRequest request = new FakeHttpServletRequest("/course/create/");
		FakeHttpServletResponse response = new FakeHttpServletResponse();
		List<Course> courseRepository = new ArrayList<Course>();

		new CreateCourse(request, response, courseRepository).execute();

		assertThat(response.statusCode(), is(HttpServletResponse.SC_OK));
		assertThat(response.message(), containsString("form"));
	}

	@Test
	public void create() throws Exception {
		FakeHttpServletRequest request = new FakeHttpServletRequest("/course/create", "POST");
		request.setParameter(Course.NAME, "Course name");
		request.setParameter(Course.ID, "123");
		request.setParameter(Course.DESCRIPTION, "Generic course description");
		request.setParameter(Course.LOCATION, "Mendrisio");
		request.setParameter(Course.START_DATE, "01.02.2017");
		request.setParameter(Course.TOTAL_SEATS, "20");
		FakeHttpServletResponse response = new FakeHttpServletResponse();
		List<Course> courseRepo = new ArrayList<Course>();

		new CreateCourse(request, response, courseRepo).execute();

		assertEquals(courseRepo.size(), 1);
	}
}
