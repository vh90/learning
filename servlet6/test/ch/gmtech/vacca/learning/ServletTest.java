package ch.gmtech.vacca.learning;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import javax.servlet.http.HttpServletResponse;

import org.junit.Test;

import ch.gmtech.vacca.learning.Servlet;

public class ServletTest {

	@Test
	public void statusOK() throws Exception {
		FakeHttpServletRequest request = new FakeHttpServletRequest("/course/");
		FakeHttpServletResponse response = new FakeHttpServletResponse();

		new Servlet().service(request, response);

		assertThat(response.statusCode(), is(HttpServletResponse.SC_OK));
	}

	@Test
	public void notFound() throws Exception {
		FakeHttpServletRequest request = new FakeHttpServletRequest("/course/whatever");
		FakeHttpServletResponse response = new FakeHttpServletResponse();

		new Servlet().service(request, response);

		assertThat(response.statusCode(), is(HttpServletResponse.SC_NOT_FOUND));
	}
}
