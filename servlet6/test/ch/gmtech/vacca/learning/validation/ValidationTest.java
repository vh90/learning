package ch.gmtech.vacca.learning.validation;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class ValidationTest {

	@Test
	public void considerSameRuleOnce() {
		Validation validation = new Validation()
				.setRules("a", new NotBlankRule(), new NotBlankRule());

		Map<String, String> fields = new HashMap<String, String>() {{
			put("a", "");
		}};

		Map<String, Set<String>> errors = validation.errors(fields);
		assertThat(errors.get("a").size(), is(1));
	}

	@Test
	public void collectOverRules() {
		Validation validation = new Validation()
				.setRules("a", new NotBlankRule(), new NumericRule());

		Map<String, String> fields = new HashMap<String, String>() {{
			put("a", "");
		}};

		Map<String, Set<String>> errors = validation.errors(fields);
		assertThat(errors.get("a").size(), is(2));
	}
}
