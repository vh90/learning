package ch.gmtech.vacca.learning.validation;

import static org.junit.Assert.*;

import org.junit.Test;

public class NumericRuleTest {

	@Test
	public void test() {
		assertFalse(new NumericRule().check(""));
		assertFalse(new NumericRule().check("  "));
		assertFalse(new NumericRule().check("1.0"));
		assertFalse(new NumericRule().check(" 10 "));
		assertTrue(new NumericRule().check("10"));
	}

}
