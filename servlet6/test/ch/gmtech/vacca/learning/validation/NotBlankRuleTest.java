package ch.gmtech.vacca.learning.validation;

import static org.junit.Assert.*;

import org.junit.Test;

public class NotBlankRuleTest {

	@Test
	public void check() {
		assertFalse(new NotBlankRule().check(""));
		assertFalse(new NotBlankRule().check(" "));
		assertTrue(new NotBlankRule().check(" a "));
	}

}
