package ch.gmtech.vacca.learning.validation;

import static org.junit.Assert.*;

import org.junit.Test;

public class LengthRuleTest {

	@Test
	public void lower() {
		LengthRule rule = new LengthRule(Operator.LOWER, 3);

		assertTrue(rule.check("ab"));
		assertFalse(rule.check("abc"));
		assertFalse(rule.check("abcd"));
	}

	@Test
	public void lowerEquals() {
		LengthRule rule = new LengthRule(Operator.LOWER_EQUALS, 3);

		assertTrue(rule.check("ab"));
		assertTrue(rule.check("abc"));
		assertFalse(rule.check("abcd"));
	}

	@Test
	public void equals() {
		LengthRule rule = new LengthRule(Operator.EQUALS, 3);

		assertFalse(rule.check("ab"));
		assertTrue(rule.check("abc"));
		assertFalse(rule.check("abcd"));
	}

	@Test
	public void greaterEquals() {
		LengthRule rule = new LengthRule(Operator.GREATER_EQUALS, 3);

		assertFalse(rule.check("ab"));
		assertTrue(rule.check("abc"));
		assertTrue(rule.check("abcd"));
	}

	@Test
	public void greater() {
		LengthRule rule = new LengthRule(Operator.GREATER, 3);

		assertFalse(rule.check("ab"));
		assertFalse(rule.check("abc"));
		assertTrue(rule.check("abcd"));
	}
}
