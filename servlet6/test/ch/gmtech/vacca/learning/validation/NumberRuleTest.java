package ch.gmtech.vacca.learning.validation;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

public class NumberRuleTest {

	@Test
	public void creatable() {
		NumberRule rule = new NumberRule(Operator.GREATER, new BigDecimal("0"));

		assertFalse(rule.check("notANumber"));
		assertTrue(rule.check("3.3"));
	}

	@Test
	public void lower() {
		NumberRule rule = new NumberRule(Operator.LOWER, new BigDecimal("0"));

		assertTrue(rule.check("-1"));
		assertFalse(rule.check("0"));
		assertFalse(rule.check("1"));
	}

	@Test
	public void lowerEquals() {
		NumberRule rule = new NumberRule(Operator.LOWER_EQUALS, new BigDecimal("0"));

		assertTrue(rule.check("-1"));
		assertTrue(rule.check("0"));
		assertFalse(rule.check("1"));
	}

	@Test
	public void equals() {
		NumberRule rule = new NumberRule(Operator.EQUALS, new BigDecimal("0"));

		assertFalse(rule.check("-1"));
		assertTrue(rule.check("0"));
		assertFalse(rule.check("1"));
	}

	@Test
	public void greaterEquals() {
		NumberRule rule = new NumberRule(Operator.GREATER_EQUALS, new BigDecimal("0"));

		assertFalse(rule.check("-1"));
		assertTrue(rule.check("0"));
		assertTrue(rule.check("1"));
	}

	@Test
	public void greater() {
		NumberRule rule = new NumberRule(Operator.GREATER, new BigDecimal("0"));

		assertFalse(rule.check("-1"));
		assertFalse(rule.check("0"));
		assertTrue(rule.check("1"));
	}
}
