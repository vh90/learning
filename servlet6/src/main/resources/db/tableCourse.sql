CREATE TABLE Course(
   id            INTEGER PRIMARY KEY,
   name          VARCHAR(255) NOT NULL,
   description   VARCHAR(255),
   location      VARCHAR(255) NOT NULL,
   startDate     DATETIME NOT NULL,
   totalSeats    NUMERIC NOT NULL
);
