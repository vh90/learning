package ch.gmtech.vacca.learning.validation;

public class LengthRule implements Rule {

	private final Operator _operator;
	private final Integer _limit;

	public LengthRule(Operator operator, Integer limit) {
		_operator = operator;
		_limit = limit;
	}

	@Override
	public boolean check(String value) {
		boolean result = false;
		if (_operator.equals(Operator.LOWER)) {
			result = value.length() < _limit;
		}
		if (_operator.equals(Operator.LOWER_EQUALS)) {
			result = value.length() <= _limit;
		}
		if (_operator.equals(Operator.EQUALS)) {
			result = value.length() == _limit;
		}
		if (_operator.equals(Operator.GREATER_EQUALS)) {
			result = value.length() >= _limit;
		}
		if (_operator.equals(Operator.GREATER)) {
			result = value.length() > _limit;
		}
		return result;
	}

	@Override
	public String message() {
		return "Length must be " + _operator + " " + _limit;
	}
}
