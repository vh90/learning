package ch.gmtech.vacca.learning.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import ch.gmtech.vacca.learning.seminar.Course;
import ch.gmtech.vacca.learning.view.Template;

public class ShowCourses implements Controller {

	private final List<Course> _courses;
	private final HttpServletResponse _response;

	public ShowCourses(List<Course> courses, HttpServletResponse response) {
		_courses = courses;
		_response = response;
	}

	@Override
	public void execute() throws Exception {
		_response.setStatus(HttpServletResponse.SC_OK);
		_response.getWriter().write(new Template().render(showCourses()));
	}

	private String showCourses() {
		String courseTable = "";
		for (Course course : _courses) {
			courseTable += "<td>" + course.id() + "</td>" +
					"<td>" + course.name() + "</td>" +
					"<td>" + course.location() + "</td>" +
					"<td>" + course.totalSeats() + "</td>" +
					"<td>" + course.startDate() + "</td>";
		}

		return "<div class='col-lg-8 col-md-8 col-sm-9'>" +
				" <table class='table table-striped'>" +
				"  <thead>" +
				"   <tr>" +
				"    <th>" + Course.ID + "</th>" +
				"    <th>" + Course.NAME + "</th>" +
				"    <th>" + Course.LOCATION + "</th>" +
				"    <th>" + Course.TOTAL_SEATS + "</th>" +
				"    <th>" + Course.START_DATE + "</th>" +
				"   </tr>" +
				"  </thead>" +
				"  <tbody>" +
				courseTable +
				"  </tbody>" +
				" </table>" +
				"</div>";
	}
}
