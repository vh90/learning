package ch.gmtech.vacca.learning.validation;

import org.apache.commons.lang3.StringUtils;

public class NotBlankRule implements Rule {

	@Override
	public boolean check(String value) {
		return StringUtils.isNotBlank(value);
	}

	@Override
	public String message() {
		return "Can't be empty";
	}
}
