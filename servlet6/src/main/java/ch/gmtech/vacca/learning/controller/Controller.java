package ch.gmtech.vacca.learning.controller;

public interface Controller {

	void execute() throws Exception;
}
