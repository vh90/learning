package ch.gmtech.vacca.learning.validation;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class Validation {

	private final Map<String, Collection<Rule>> _rules = new HashMap<String, Collection<Rule>>();

	public Validation setRules(String key, Rule... rules) {
		_rules.put(key, Arrays.asList(rules));
		return this;
	}

	public Map<String, Set<String>> errors(Map<String, String> fields) {
		Map<String, Set<String>> validationErrors = new HashMap<String, Set<String>>();
		for (String key : _rules.keySet()) {
			Set<String> errors = new LinkedHashSet<String>();
			for (Rule rule : _rules.get(key)) {
				if (! rule.check(fields.get(key))) {
					errors.add(rule.message());
				}
			}
			validationErrors.put(key, errors);
		}
		return validationErrors;
	}

	public boolean isValid(Map<String, String> fields) {
		Integer numberOfErrors = 0;
		for (Set<String> fieldErrors : errors(fields).values()) {
			numberOfErrors += fieldErrors.size();
		}

		return numberOfErrors == 0;
	}

}
