package ch.gmtech.vacca.learning.validation;

import ch.gmtech.vacca.learning.util.Time;

public class DateRule implements Rule {

	@Override
	public boolean check(String value) {
		
		try {
			new Time(value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public String message() {
		return "Must be a valid date";
	}

}
