package ch.gmtech.vacca.learning.validation;

import java.math.BigDecimal;

import org.apache.commons.lang3.math.NumberUtils;

public class NumberRule implements Rule {

	private final Operator _operator;
	private final BigDecimal _limit;

	public NumberRule(Operator operator, BigDecimal limit) {
		_operator = operator;
		_limit = limit;
	}

	@Override
	public boolean check(String value) {
		if (NumberUtils.isCreatable(value)) {
			if (_operator.equals(Operator.LOWER)) {
				return new BigDecimal(value).compareTo(_limit) < 0;
			}
			if (_operator.equals(Operator.LOWER_EQUALS)) {
				return new BigDecimal(value).compareTo(_limit) <= 0;
			}
			if (_operator.equals(Operator.EQUALS)) {
				return new BigDecimal(value).compareTo(_limit) == 0;
			}
			if (_operator.equals(Operator.GREATER_EQUALS)) {
				return new BigDecimal(value).compareTo(_limit) >= 0;
			}
			if (_operator.equals(Operator.GREATER)) {
				return new BigDecimal(value).compareTo(_limit) > 0;
			}
		}
		return false;
	}

	@Override
	public String message() {
		return "Value must be " + _operator + " " + _limit;
	}
}
