package ch.gmtech.vacca.learning.validation;

import org.apache.commons.lang3.math.NumberUtils;

public class NumericRule implements Rule {

	@Override
	public boolean check(String value) {
		return NumberUtils.isDigits(value);
	}

	@Override
	public String message() {
		return "Must contains only digits";
	}

}
