package ch.gmtech.vacca.learning.view;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class Form {

	private final Map<String, String> _fields = new LinkedHashMap<String, String>();

	public Form(LinkedHashMap<String, String> fields) {
		for (String field : fields.keySet()) {
			_fields.put(field, fields.get(field));
		}
	}

	public String render() {
		return render(new HashMap<String, Set<String>>());
	}

	public String render(Map<String, Set<String>> validationErrors) {

		String html = "<div class='col-md-6 col-md-offset-3'>" +
				"<h1 class='page-header text-center'>Insert Course</h1>" +
				"<form class='form-horizontal' role='form' method='post'>";

		for (String field : _fields.keySet()) {
			if (validationErrors.containsKey(field)) {
				html += validatedDiv(field, _fields.get(field), validationErrors.get(field));
			} else {
				html += inputDiv(field, _fields.get(field));
			}
		}

		html += "<div class='form-group'>" +
					"<div class='col-sm-10 col-sm-offset-2'>" +
						"<input id='submit' name='submit' type='submit' value='Send' class='btn btn-primary'>" +
					"</div>" +
				"</div>" +
			"</form>" +
		"</div>";

		return new Template().render(html);
	}

	private String inputDiv(String field, String value) {
		return "<div class='form-group'>" +
				"<label for='" + field + "' class='col-sm-2 control-label'>" + field + "</label>" +
				"<div class='col-sm-10'>" +
				"<input type='text' class='form-control' id='" + field + "' name='" + field + "' placeholder='" + field + (value==null ? "" : ("' value='" + value)) + "'/>" +
				"</div>" +
				"</div>";
	}

	private String validatedDiv(String field, String value, Set<String> errors) {

		String validationStyle = errors.isEmpty() ? "has-success" : "has-error";

		String div = "<div class='form-group " + validationStyle + "'>" +
				"<label for='" + field + "' class='col-sm-2 control-label'>" + field + "</label>" +
				"<div class='col-sm-10'>" +
				"<input type='text' class='form-control' id='" + field + "' name='" + field + "' placeholder='" + field + (value==null ? "" : ("' value='" + value)) + "'/>";
		for (String error : errors) {
			div += "<span class='help-block'>" + error + "</span>";
		}

		return div + "</div>" + "</div>";
	}
}
