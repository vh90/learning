package ch.gmtech.vacca.learning.validation;

public enum Operator {

	LOWER("lower than"),
	LOWER_EQUALS("lower than or equals to"),
	EQUALS("equals to"),
	GREATER_EQUALS("greater than or equals to"),
	GREATER("greater than");

	private final String _representation;

	Operator(String representation) {
		_representation = representation;
	}

	@Override
	public String toString() {
		return _representation;
	}
}
