package ch.gmtech.vacca.learning.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.gmtech.vacca.learning.Servlet;
import ch.gmtech.vacca.learning.seminar.Course;
import ch.gmtech.vacca.learning.util.Time;
import ch.gmtech.vacca.learning.validation.DateRule;
import ch.gmtech.vacca.learning.validation.LengthRule;
import ch.gmtech.vacca.learning.validation.NotBlankRule;
import ch.gmtech.vacca.learning.validation.NumberRule;
import ch.gmtech.vacca.learning.validation.NumericRule;
import ch.gmtech.vacca.learning.validation.Operator;
import ch.gmtech.vacca.learning.validation.Validation;
import ch.gmtech.vacca.learning.view.Form;

public class CreateCourse implements Controller {

	private final HttpServletRequest _request;
	private final HttpServletResponse _response;
	private final List<Course> _courses;

	private final static List<String> FIELDS_TO_SHOW = Arrays.asList(
			Course.ID,
			Course.NAME,
			Course.DESCRIPTION,
			Course.LOCATION,
			Course.START_DATE,
			Course.TOTAL_SEATS
		);

	public CreateCourse(HttpServletRequest request, HttpServletResponse response, List<Course> courses) {
		_request = request;
		_response = response;
		_courses = courses;
	}

	@Override
	public void execute() throws Exception {
		if ("POST".equals(_request.getMethod())) {
			action();
		}

		if ("GET".equals(_request.getMethod())) {
			_response.setStatus(HttpServletResponse.SC_OK);
			_response.getWriter().write(new Form(parameters()).render());
		}
	}

	private void action() throws IOException {

		Validation validation = new Validation()
				.setRules(Course.ID,
						new NotBlankRule(),
						new NumericRule()
					)
				.setRules(Course.NAME,
						new NotBlankRule(),
						new LengthRule(Operator.LOWER_EQUALS, 15)
					)
				.setRules(Course.LOCATION,
						new NotBlankRule(),
						new LengthRule(Operator.LOWER_EQUALS, 20)
					)
				.setRules(Course.START_DATE,
						new NotBlankRule(),
						new DateRule()
					)
				.setRules(Course.TOTAL_SEATS,
						new NotBlankRule(),
						new NumberRule(Operator.GREATER, BigDecimal.ZERO),
						new LengthRule(Operator.LOWER_EQUALS, 3)
						//TODO IntegerRule?
					);

		if (validation.isValid(parameters())) {
			Course newCourse = new Course(
					Integer.parseInt(_request.getParameter(Course.ID)),
					_request.getParameter(Course.NAME),
					_request.getParameter(Course.DESCRIPTION),
					_request.getParameter(Course.LOCATION),
					new Time(_request.getParameter(Course.START_DATE)),
					Integer.parseInt(_request.getParameter(Course.TOTAL_SEATS))
				);
			_courses.add(newCourse);

			_response.sendRedirect(Servlet.SHOW_COURSES);
		} else {
			_response.setStatus(HttpServletResponse.SC_OK);
			_response.getWriter().write(new Form(parameters()).render(validation.errors(parameters())));
		}
		
	}

	private LinkedHashMap<String, String> parameters() {
		
		LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
		for (String key : FIELDS_TO_SHOW) {
			parameters.put(key, _request.getParameter(key));
		}
		return parameters;
	}
}
