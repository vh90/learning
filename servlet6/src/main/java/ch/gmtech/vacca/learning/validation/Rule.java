package ch.gmtech.vacca.learning.validation;

public interface Rule {

	boolean check(String value);

	String message();
}
