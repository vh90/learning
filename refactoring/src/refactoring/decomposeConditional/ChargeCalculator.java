package refactoring.decomposeConditional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChargeCalculator {

	private final double _winterRate;
	private final double _winterServiceCharge;
	private final double _summerRate;

	public ChargeCalculator(double winterRate, double winterServiceCharge, double summerRate) {
		_winterRate = winterRate;
		_winterServiceCharge = winterServiceCharge;
		_summerRate = summerRate;
	}

	double calculate(Date date) {
		int quantity = 0;
		return notSummer(date)
			? winterCharge(quantity)
			: summerCharge(quantity);
	}

	private double summerCharge(int quantity) {
		return quantity * _summerRate;
	}

	private double winterCharge(int quantity) {
		return quantity * _winterRate + _winterServiceCharge;
	}

	private boolean notSummer(Date date) {
		return date.before(summerStart()) || date.after(summerEnd());
	}

	private Date summerEnd() {
		return parse("21.09");
	}

	private Date summerStart() {
		return parse("21.06");
	}

	private Date parse(String source) {
		try {
			return new SimpleDateFormat("dd.MM").parse(source);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
}
