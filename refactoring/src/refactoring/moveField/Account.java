package refactoring.moveField;

class Account {

	private AccountType _type;

	double interestForAmount_days(double amount, int days) {
		return getInterestRate() * amount * days / 365;
	}

	public AccountType getType() {
		return _type;
	}

	public void setType(AccountType type) {
		_type = type;
	}

	public double getInterestRate() {
		return getType().getInterestRate();
	}

	public void setInterestRate(double interestRate) {
		getType().setInterestRate(interestRate);
	}
}
