package refactoring.moveField;

class AccountType {

	private double _interestRate;

	public double getInterestRate() {
		return _interestRate;
	}

	public void setInterestRate(double interestRate) {
		_interestRate = interestRate;
	}
}
