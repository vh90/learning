package refactoring.extractClass;

public class TelephoneNumber {

	private String _areaCode;
	private String _number;

	public String getAreaCode() {
		return _areaCode;
	}

	public void setAreaCode(String areaCode) {
		_areaCode = areaCode;
	}

	String getNumber() {
		return _number;
	}

	void setNumber(String arg) {
		_number = arg;
	}

	public String getTelephoneNumber() {
		return ("(" + _areaCode + ") " + _number);
	}

}
