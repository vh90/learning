package refactoring.extractClass;

class Person {

	public String getName() {
		return _name;
	}

	/**
	 * @deprecated Use {@link refactoring.extractClass.TelephoneNumber#getTelephoneNumber()} instead
	 */
	@Deprecated
	public String getTelephoneNumber() {
		return _officeTelephone.getTelephoneNumber();
	}

	/**
	 * Who have the responsibility to manipulate the informations?
	 */
	String getOfficeAreaCode() {
		return _officeTelephone.getAreaCode();
	}

	void setOfficeAreaCode(String arg) {
		_officeTelephone.setAreaCode(arg);
	}

	String getOfficeNumber() {
		return _officeTelephone.getNumber();
	}

	void setOfficeNumber(String arg) {
		_officeTelephone.setNumber(arg);
	}

	private String _name;
	private final TelephoneNumber _officeTelephone = new TelephoneNumber();
}
