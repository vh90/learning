package refactoring.introduceParameterObject;

import java.util.Date;

class DateRange {

	private final Date _start;
	private final Date _end;

	DateRange(Date start, Date end) {
		_start = start;
		_end = end;
	}

	Date start() {
		return _start;
	}

	Date end() {
		return _end;
	}

	boolean contains(Date actual) {
		return actual.equals(_start)
				|| actual.equals(_end)
				|| (actual.after(_start) && actual.before(_end));
	}
}
