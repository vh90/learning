package refactoring.replaceTempWithQuery;

public class PriceCalculator {

	private int _quantity;
	private int _itemPrice;

	double getPrice() {
		return basePrice() * discountFactor();
	}

	/*
	 * If basePrice weren't extracted, I would pass the value as parameter...
	 */
	private double discountFactor() {
		if (basePrice() > 1000) {
			return 0.95;
		} else {
			return 0.98;
		}
	}

	private int basePrice() {
		return _quantity * _itemPrice;
	}
}
