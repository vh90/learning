package refactoring.hideDelegate;

class Person {

	private Department _department;
	private final String _name;

	public Person(String name) {
		_name = name;
	}

	public Department getDepartment() {
		return _department;
	}

	public void setDepartment(Department arg) {
		_department = arg;
	}

	public String getName() {
		return _name;
	}

	Person getManager() {
		return _department.getManager();
	}
}
