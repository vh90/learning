package refactoring.replaceTypeCodeWithStateStrategy;

class Manager implements EmployeeType {

	@Override
	public int typeCode() {
		return Employee.MANAGER;
	}

}
