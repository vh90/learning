package refactoring.replaceTypeCodeWithStateStrategy;

class Salesman implements EmployeeType {

	@Override
	public int typeCode() {
		return Employee.SALESMAN;
	}

}
