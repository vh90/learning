package refactoring.replaceTypeCodeWithStateStrategy;

interface EmployeeType {

	int typeCode();
}
