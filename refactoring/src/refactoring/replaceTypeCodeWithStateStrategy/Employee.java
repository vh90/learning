package refactoring.replaceTypeCodeWithStateStrategy;

class Employee {

	private EmployeeType _type;

	private int _monthlySalary;
	private int _commission;
	private int _bonus;

	static final int ENGINEER = 0;
	static final int SALESMAN = 1;
	static final int MANAGER = 2;

	Employee(int type) {
		setType(type);
	}

	int getType() {
		return _type.typeCode();
	}

	void setType(int type) {
		switch (type) {
		case ENGINEER:
			_type = new Engineer();
		case SALESMAN:
			_type = new Salesman();
		case MANAGER:
			_type = new Manager();
		default:
			throw new RuntimeException("Incorrect Employee");
		}
	}

	int payAmount() {
		switch (getType()) {
			case ENGINEER:
				return _monthlySalary;
			case SALESMAN:
				return _monthlySalary + _commission;
			case MANAGER:
				return _monthlySalary + _bonus;
			default:
				throw new RuntimeException("Incorrect Employee");
		}
	}
}
