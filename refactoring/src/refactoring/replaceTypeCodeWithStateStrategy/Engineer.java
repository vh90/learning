package refactoring.replaceTypeCodeWithStateStrategy;

class Engineer implements EmployeeType {

	@Override
	public int typeCode() {
		return Employee.ENGINEER;
	}

}
