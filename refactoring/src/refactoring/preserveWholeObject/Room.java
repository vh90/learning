package refactoring.preserveWholeObject;

public class Room {
	private TempRange _tempRange;

	boolean withinPlan(HeatingPlan plan) {
		return plan.withinRange(daysTempRange());
	}

	private TempRange daysTempRange() {
		return _tempRange;
	}
}
