package refactoring.preserveWholeObject;

public class TempRange {

	private int _low;
	private int _high;

	public int getLow() {
		return _low;
	}

	public int getHigh() {
		return _high;
	}

	public boolean includes(TempRange other) {
		return other.getLow() >= getLow() && other.getHigh() <= getHigh();
	}

}
