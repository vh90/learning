package refactoring.extractSubclass;

class JobItem {

	private final int _unitPrice;
	private final int _quantity;

	JobItem (int unitPrice, int quantity) {
		_unitPrice = unitPrice;
		_quantity = quantity;
	}

	int getTotalPrice() {
		return getUnitPrice() * _quantity;
	}

	int getUnitPrice(){
		return _unitPrice;
	}

	int getQuantity(){
		return _quantity;
	}
}
