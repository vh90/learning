package refactoring.extractSubclass;

class EmployeeItem extends JobItem {

	private final Employee _employee;

	EmployeeItem(int unitPrice, int quantity, Employee employee) {
		super(unitPrice, quantity);
		_employee = employee;
	}

	Employee getEmployee() {
		return _employee;
	}

	@Override
	int getUnitPrice() {
		return _employee.getRate();
	}
}
