package refactoring.replaceArrayWithObject;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class PerformanceTest {

	@Test
	public void test() {
		Performance performance = new Performance("Liverpool", 15);

		assertThat(performance.getName(), is("Liverpool"));
		assertThat(performance.getWins(), is(15));
	}
}
