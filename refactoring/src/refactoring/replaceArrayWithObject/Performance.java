package refactoring.replaceArrayWithObject;

class Performance {

	private String _name;
	private Integer _wins;

	public Performance(String name, Integer wins) {
		_name = name;
		_wins = wins;
	}

	String getName() {
		return _name;
	}

	void setName(String name) {
		_name = name;
	}

	Integer getWins() {
		return _wins;
	}

	void setWins(Integer wins) {
		_wins = wins;
	}
}
