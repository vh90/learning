package refactoring.encapsulateCollection;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

class Person {

	private final Set<Course> _courses = new HashSet<Course>();

	boolean addCourse(Course course) {
		return _courses.add(course);
	}

	boolean removeCourse(Course course) {
		return _courses.remove(course);
	}

	int numberOfAdvancedCourses() {
		int count = 0;
		for (Course each : Collections.unmodifiableSet(_courses)) {
			if (each.isAdvanced()) {
				count ++;
			}
		}
		return count;
	}

	int numberOfCourses() {
		return Collections.unmodifiableSet(_courses).size();
	}
}
