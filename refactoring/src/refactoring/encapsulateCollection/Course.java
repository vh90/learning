package refactoring.encapsulateCollection;

class Course {

	private final String _name;
	private final boolean _isAdvanced;

	Course (String name, boolean isAdvanced) {
		_name = name;
		_isAdvanced = isAdvanced;
		
	};

	boolean isAdvanced() {
		return _isAdvanced;
	}

	String getName() {
		return _name;
	}
}
