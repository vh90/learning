package refactoring.encapsulateCollection;

import static org.junit.Assert.*;

import org.junit.Test;

public class EncapsusateCollectionTest {

	@Test
	public void test() {
		Person kent = new Person();

		kent.addCourse(new Course ("Smalltalk Programming", false));
		kent.addCourse(new Course ("Appreciating Single Malts", true));
		assertEquals(2, kent.numberOfCourses());

		Course refact = new Course ("Refactoring", true);
		kent.addCourse(refact);
		kent.addCourse(new Course ("Brutal Sarcasm", false));
		assertEquals(4, kent.numberOfCourses());

		kent.removeCourse(refact);
		assertEquals(3, kent.numberOfCourses());
	}

	@Test
	public void advanced() {
		Person kent = new Person();
		kent.addCourse(new Course ("Smalltalk Programming", false));
		kent.addCourse(new Course ("Appreciating Single Malts", true));

		assertEquals(1, kent.numberOfAdvancedCourses());
	}
}
