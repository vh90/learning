package refactoring.formTemplateMethod;

class Rental {

	private final Movie _movie;
	private final int _daysRented;
	private final double _charge;

	public Rental(Movie movie, double charge, int daysRented) {
		_movie = movie;
		_charge = charge;
		_daysRented = daysRented;
	}

	public Movie getMovie() {
		return _movie;
	}

	public int getDaysRented() {
		return _daysRented;
	}

	public double getCharge() {
		return _charge;
	}

	int getFrequentRenterPoints() {
		if ((getMovie().getPriceCode() == Movie.NEW_RELEASE)
				&& getDaysRented() > 1)
			return 2;
		else
			return 1;
	}
}
