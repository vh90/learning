package refactoring.formTemplateMethod;

import java.util.Enumeration;

abstract class Statement {

	protected String value(Customer customer) {
		Enumeration rentals = customer.rentals().elements();
		String result = header(customer);
		while (rentals.hasMoreElements()) {
			Rental each = (Rental) rentals.nextElement();
			// show figures for this rental
			result += eachRentalString(each);
		}
		// add footer lines
		result += footer(customer);
		return result;
	}

	protected abstract String header(Customer customer);
	protected abstract String eachRentalString(Rental each);
	protected abstract String footer(Customer customer);

}
