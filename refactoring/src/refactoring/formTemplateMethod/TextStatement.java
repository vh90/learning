package refactoring.formTemplateMethod;


public class TextStatement extends Statement {

	@Override
	protected String eachRentalString(Rental each) {
		return "\t" + each.getMovie().getTitle() + "\t"
				+ String.valueOf(each.getCharge()) + "\n";
	}

	@Override
	protected String footer(Customer customer) {
		return "Amount owed is " + String.valueOf(customer.getTotalCharge()) + "\n"
				+ "You earned "
				+ String.valueOf(customer.getTotalFrequentRenterPoints())
				+ " frequent renter points";
	}

	@Override
	protected String header(Customer customer) {
		return "Rental Record for " + customer.getName() + "\n";
	}
}
