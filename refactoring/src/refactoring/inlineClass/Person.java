package refactoring.inlineClass;

class Person {
	public String getName() {
		return _name;
	}

	public String getTelephoneNumber() {
		return "(" + _areaCode + ") " + _number;
	}

	String getAreaCode() {
		return _areaCode;
	}

	void setAreaCode(String arg) {
		_areaCode = arg;
	}

	String getNumber() {
		return _number;
	}

	void setNumber(String arg) {
		_number = arg;
	}

	private final String _name;
	private String _number;
	private String _areaCode;

	public Person(String name, String number, String areaCode) {
		_name = name;
		_number = number;
		_areaCode = areaCode;
	}
}
