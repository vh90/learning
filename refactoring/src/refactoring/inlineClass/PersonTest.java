package refactoring.inlineClass;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PersonTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		Person person = new Person("pippo", "0910123456", "+41");
		assertEquals(person.getTelephoneNumber(), "(+41) 0910123456");
	}

}
