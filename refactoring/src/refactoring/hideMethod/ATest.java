package refactoring.hideMethod;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class ATest {

	@Test
	public void test() {
		assertThat(new A(0.2).x(3, 1), is(3.2));
	}

}
