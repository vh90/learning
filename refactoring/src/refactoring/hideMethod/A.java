package refactoring.hideMethod;

class A {

	private final double _rate;

	A(double rate) {
		_rate = rate;
	}

	double x(int a, int b) {
		return a + rate(b);
	}

	// before it was public
	private double rate(int b) {
		return _rate * b;
	}
}
