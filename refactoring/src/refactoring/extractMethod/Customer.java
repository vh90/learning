package refactoring.extractMethod;

import java.util.Enumeration;
import java.util.Vector;

class Customer {

	private final Vector<Order> _orders;
	private final String _name;

	public Customer(Vector<Order> orders, String name) {
		_orders = orders;
		_name = name;
	}

	void printOwing() {

		printBanner();

		double outstanding = getOutstanding();

		printDetails(outstanding);
	}

	private double getOutstanding() {
		Enumeration<Order> e = _orders.elements();
		double outstanding = 0.0;
		// calculate outstanding
		while (e.hasMoreElements()) {
			Order each = e.nextElement();
			outstanding += each.getAmount();
		}
		return outstanding;
	}

	private void printBanner() {
		// print banner
		System.out.println ("**************************");
		System.out.println ("***** Customer Owes ******");
		System.out.println ("**************************");
	}
	
	private void printDetails(double outstanding) {
		//print details
		System.out.println ("name:" + _name);
		System.out.println ("amount" + outstanding);
	}

	//-------------------------------------------------------------------------

	void pringOwing(double previousAmount) {

		printBanner();

		double outstanding = getOutstanding(previousAmount * 1.2);

		printDetails(outstanding);
	}

	private double getOutstanding(double initialValue) {
		Enumeration<Order> e = _orders.elements();
		double outstanding = initialValue;
		// calculate outstanding
		while (e.hasMoreElements()) {
			Order each = e.nextElement();
			outstanding += each.getAmount();
		}
		return outstanding;
	}
}
