package refactoring.extractMethod;

class Order {

	private final double _amount;

	public Order(double amount) {
		_amount = amount;
	}

	public double getAmount() {
		return _amount;
	}
	
}