package refactoring.removeMiddleMan;

import static org.junit.Assert.*;

import org.junit.Test;

public class MiddleManTest {

	@Test
	public void test() {
		Department department = new Department(new Person("The boss"));

		Person john = new Person("John");
		john.setDepartment(department);

		Person manager = john.getDepartment().getManager();
		assertEquals(manager.getName(), "The boss");
	}

}
