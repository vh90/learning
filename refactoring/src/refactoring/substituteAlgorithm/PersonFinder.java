package refactoring.substituteAlgorithm;

import java.util.Arrays;
import java.util.List;

public class PersonFinder {

	String foundPerson(String[] people){
		List<String> candidates = Arrays.asList("Don", "John", "Kent");
		for (String person : people) {
			if (candidates.contains(person)) {
				return person;
			}
		}

		return "";
	}
}
