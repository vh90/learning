package refactoring.introduceLocalExtension;

import java.util.Date;

class StartDayPrinterUsingWrapper {

	private final MfDateWrapper _previousEnd;

	StartDayPrinterUsingWrapper(Date previousEnd) {
		_previousEnd = new MfDateWrapper(previousEnd);
	}

	String printStartOnNextDay() {
		Date newStart = _previousEnd.nextDay();
		return "Start on " + newStart;
	}
}
