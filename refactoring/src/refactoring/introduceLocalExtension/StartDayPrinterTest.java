package refactoring.introduceLocalExtension;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class StartDayPrinterTest {

	@Test
	public void testSub() {
		assertThat(new StartDayPrinterUsingSub(new Date(0)).printStartOnNextDay(), is("Start on Fri Jan 02 00:00:00 CET 1970"));
	}

	@Test
	public void testWrapper() {
		assertThat(new StartDayPrinterUsingWrapper(new Date(0)).printStartOnNextDay(), is("Start on Fri Jan 02 00:00:00 CET 1970"));
	}

}
