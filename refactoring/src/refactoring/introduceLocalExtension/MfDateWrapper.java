package refactoring.introduceLocalExtension;

import java.util.Date;

class MfDateWrapper {

	private final Date _previousEnd;

	public MfDateWrapper(Date previousEnd) {
		_previousEnd = previousEnd;
	}

	public Date nextDay() {
		return new Date(_previousEnd.getYear(), _previousEnd.getMonth(), _previousEnd.getDate() + 1);
	}

	//I should add delegating methods in order to simulate the behavior of java.util.Date ...
}
