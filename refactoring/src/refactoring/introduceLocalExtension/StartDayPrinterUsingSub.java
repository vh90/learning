package refactoring.introduceLocalExtension;

import java.util.Date;

class StartDayPrinterUsingSub {

	private final MfDateSub _previousEnd;

	StartDayPrinterUsingSub(Date previousEnd) {
		_previousEnd = new MfDateSub(previousEnd);
	}

	String printStartOnNextDay() {
		Date newStart = _previousEnd.nextDay();
		return "Start on " + newStart;
	}
}
