package refactoring.introduceLocalExtension;

import java.util.Date;

class MfDateSub extends Date {

	MfDateSub(String dateString) {
		super(dateString);
	}

	MfDateSub(Date arg) {
		super(arg.getTime());
	}

	Date nextDay() {
		return new Date(getYear(), getMonth(), getDate() + 1);
	}

	//Here I don't have to override anything, just because the method are inherited from super...
}
