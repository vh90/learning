package refactoring.changeBidirectionalAssociationToUnidirectional;

import java.util.HashSet;
import java.util.Set;


class Customer {

	private final Set<Order> _orders = new HashSet<Order>();
	private int _discount;

	Set<Order> friendOrders() {
		/** should only be used by Order */
		return _orders;
	}

	double getPriceFor(Order order) {
		assert _orders.contains(order); // see Introduce Assertion (267)
		return order.getDiscountedPrice(this);
	}

	public int getDiscount() {
		return _discount;
	}

}
