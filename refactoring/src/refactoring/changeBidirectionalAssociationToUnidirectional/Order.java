package refactoring.changeBidirectionalAssociationToUnidirectional;

class Order {

	private int _grossPrice;

	double getDiscountedPrice(Customer customer) {
		return getGrossPrice() * (1 - customer.getDiscount());
	}

	private int getGrossPrice() {
		return _grossPrice;
	}
}
