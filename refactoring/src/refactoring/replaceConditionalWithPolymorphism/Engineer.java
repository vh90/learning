package refactoring.replaceConditionalWithPolymorphism;

class Engineer implements EmployeeType {

	@Override
	public int typeCode() {
		return ENGINEER;
	}

	@Override
	public int payAmount(Employee employee) {
		return employee.getMonthlySalary();
	}
}
