package refactoring.replaceConditionalWithPolymorphism;

class Salesman implements EmployeeType {

	@Override
	public int typeCode() {
		return SALESMAN;
	}

	@Override
	public int payAmount(Employee employee) {
		return employee.getMonthlySalary() + employee.getCommission();
	}
}
