package refactoring.replaceConditionalWithPolymorphism;

class Manager implements EmployeeType {

	@Override
	public int typeCode() {
		return MANAGER;
	}

	@Override
	public int payAmount(Employee employee) {
		return employee.getMonthlySalary() + employee.getBonus();
	}
}
