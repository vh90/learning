package refactoring.replaceConditionalWithPolymorphism;

class Employee {

	private EmployeeType _type;

	private int _monthlySalary;
	private int _commission;
	private int _bonus;

	Employee(int type) {
		setType(type);
	}

	int getType() {
		return _type.typeCode();
	}

	void setType(int type) {
		switch (type) {
		case EmployeeType.ENGINEER:
			_type = new Engineer();
		case EmployeeType.SALESMAN:
			_type = new Salesman();
		case EmployeeType.MANAGER:
			_type = new Manager();
		default:
			throw new RuntimeException("Incorrect Employee");
		}
	}

	int payAmount() {
		return _type.payAmount(this);
	}

	public int getMonthlySalary() {
		return _monthlySalary;
	}

	public int getCommission() {
		return _commission;
	}

	public int getBonus() {
		return _bonus;
	}
}
