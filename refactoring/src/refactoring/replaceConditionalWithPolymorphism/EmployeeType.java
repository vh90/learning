package refactoring.replaceConditionalWithPolymorphism;

interface EmployeeType {

	static final int ENGINEER = 0;
	static final int SALESMAN = 1;
	static final int MANAGER = 2;

	int typeCode();
	int payAmount(Employee employee);
}
