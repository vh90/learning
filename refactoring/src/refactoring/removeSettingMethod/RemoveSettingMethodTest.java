package refactoring.removeSettingMethod;

import static org.junit.Assert.*;

import org.junit.Test;

public class RemoveSettingMethodTest {

	@Test
	public void test() {
		assertEquals("ZZ", new Account().toString());
		assertEquals("ZZ123", new Account("123").toString());

		assertEquals("ZZ456 rate:7.8", new InterestAccount("456", 7.8).toString());
	}
}
