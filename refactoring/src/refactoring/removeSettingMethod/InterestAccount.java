package refactoring.removeSettingMethod;

class InterestAccount extends Account {

	private final double _interestRate;

	InterestAccount (String id, double rate) {
		super(id);
		_interestRate = rate;
	}

	@Override
	public String toString() {
		return super.toString() + " rate:" + _interestRate;
	}

}
