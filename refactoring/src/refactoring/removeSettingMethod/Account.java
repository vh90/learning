package refactoring.removeSettingMethod;

class Account {

	private final String _id;

	Account() {
		this("");
	}

	Account (String id) {
		_id = "ZZ" + id;
	}

	@Override
	public String toString() {
		return _id;
	}
}
