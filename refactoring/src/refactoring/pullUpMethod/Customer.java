package refactoring.pullUpMethod;

import java.util.Date;

abstract class Customer {

	protected Date _lastBillDate = new Date();
	private double _total = 0.0;

	void createBill(Date date) {
		double chargeAmount = chargeFor(_lastBillDate, date);
		addBill (date, chargeAmount);
	}
	
	void addBill(Date dat, double amount) {
		_total += amount;
		_lastBillDate = dat;
	}

	abstract double chargeFor(Date start, Date end);
	
	public double totalDebt() {
		return _total;
	}
}
