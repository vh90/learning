package refactoring.pullUpMethod;

import java.util.Date;

public class PreferredCustomer extends Customer {

	@Override
	double chargeFor(Date start, Date end) {
		return 1.5*(end.getTime() - start.getTime())/10000000;
	}
}
