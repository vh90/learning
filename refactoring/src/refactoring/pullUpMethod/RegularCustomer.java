package refactoring.pullUpMethod;

import java.util.Date;

public class RegularCustomer extends Customer {

	@Override
	double chargeFor(Date start, Date end) {
		return 2*(end.getTime() - start.getTime())/10000000;
	}
}
