package refactoring.replaceMethodWithMethodObject;

public class Gamma {

	private final Account _account;
	private final int _inputVal;
	private final int _quantity;
	private final int _yearToDate;
	private int _importantValue2;
	private int _importantValue3;

	public Gamma(Account account, int inputVal, int quantity, int yearToDate) {
		_account = account;
		_inputVal = inputVal;
		_quantity = quantity;
		_yearToDate = yearToDate;
	}

	int compute() {
		_importantValue2 = (_inputVal * _yearToDate) + 100;
		//Fowler suggest to extract the conditional to method
		if ((_yearToDate - importantValue1()) > 100) {
			_importantValue2 -= 20;
		}
		_importantValue3 = _importantValue2 * 7;
		// and so on.
		return _importantValue3 - 2 * importantValue1();
	}

	private int importantValue1() {
		return (_inputVal * _quantity) + _account.delta();
	}
}
