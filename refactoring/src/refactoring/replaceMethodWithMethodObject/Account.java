package refactoring.replaceMethodWithMethodObject;

public class Account {

	private int _delta;

	int gamma(int inputVal, int quantity, int yearToDate) {
		return new Gamma(this, inputVal, quantity, yearToDate).compute();
	}

	int delta() {
		return _delta;
	}
}
