package refactoring.replaceDelegationWithInheritance;

class Employee extends Person {

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public void setName(String arg) {
		super.setName(arg);
	}

	@Override
	public String toString() {
		return "Emp: " + super.getLastName();
	}
}
