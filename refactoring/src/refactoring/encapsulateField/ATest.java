package refactoring.encapsulateField;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class ATest {

	@Test
	public void test() {
		A a = new A("pippo");
		assertThat(a.getName(), is("pippo"));

		a.setName("pluto");
		assertThat(a.getName(), is("pluto"));
	}

}
