package refactoring.encapsulateField;

class A {

	private String _name;

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public A(String name) {
		_name = name;
	}
}
