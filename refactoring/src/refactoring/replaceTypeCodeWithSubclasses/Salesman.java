package refactoring.replaceTypeCodeWithSubclasses;

class Salesman extends Employee {

	@Override
	public int getType() {
		return SALESMAN;
	}
}
