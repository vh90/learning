package refactoring.replaceTypeCodeWithSubclasses;

class Engineer extends Employee {

	@Override
	public int getType() {
		return ENGINEER;
	}
}
