package refactoring.replaceTypeCodeWithSubclasses;

class Manager extends Employee {

	@Override
	public int getType() {
		return MANAGER;
	}
}
