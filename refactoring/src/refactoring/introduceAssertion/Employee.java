package refactoring.introduceAssertion;

import static org.junit.Assert.*;


public class Employee {

	private static final double NULL_EXPENSE = -1.0;
	private double _expenseLimit = NULL_EXPENSE;
	private Project _primaryProject;

	/* Before:
		Exception in thread "main" java.lang.NullPointerException
			at refactoring.introduceAssertion.Employee.getExpenseLimit(Employee.java:12)
			at refactoring.introduceAssertion.Employee.main(Employee.java:24)
	 */
	/* After:
		Exception in thread "main" java.lang.AssertionError
			at org.junit.Assert.fail(Assert.java:86)
			at org.junit.Assert.assertTrue(Assert.java:41)
			at org.junit.Assert.assertTrue(Assert.java:52)
			at refactoring.introduceAssertion.Employee.getExpenseLimit(Employee.java:18)
			at refactoring.introduceAssertion.Employee.main(Employee.java:33)
	 */
	double getExpenseLimit() {
		assertTrue(_expenseLimit != NULL_EXPENSE || _primaryProject != null);
		return _expenseLimit != NULL_EXPENSE
				? _expenseLimit
				: _primaryProject.getMemberExpenseLimit();
	}

	boolean withinLimit(double expenseAmount) {
		return (expenseAmount <= getExpenseLimit());
	}

	void setExpenseLimit(double expenseLimit) {
		_expenseLimit = expenseLimit;
	}

	public static void main(String[] args) {
		double expenseLimit = new Employee().getExpenseLimit();
		System.out.println(expenseLimit);
	}
}
