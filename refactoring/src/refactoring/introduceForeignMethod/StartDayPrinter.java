package refactoring.introduceForeignMethod;

import java.util.Date;

class StartDayPrinter {

	private final Date _previousEnd;

	StartDayPrinter(Date previousEnd) {
		_previousEnd = previousEnd;
	}

	String printStartOnNextDay() {
		Date newStart = nextDay(_previousEnd);
		return "Start on " + newStart;
	}

	//foreign method; should be in java.util.Date
	private static Date nextDay(Date date) {
		return new Date(date.getYear(), date.getMonth(), date.getDate() + 1);
	}
}
