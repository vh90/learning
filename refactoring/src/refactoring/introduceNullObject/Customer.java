package refactoring.introduceNullObject;

class Customer {

	private final String _name;
	private final BillingPlan _plan;
	private final PaymentHistory _history;

	Customer(String name, BillingPlan plan, PaymentHistory history) {
		_name = name;
		_plan = plan;
		_history = history;
	}

	String getName() {
		return _name;
	}

	BillingPlan getPlan() {
		return _plan;
	}

	PaymentHistory getHistory() {
		return _history;
	}
}
