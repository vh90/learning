package refactoring.introduceNullObject;

public class PaymentHistory {

	private final int _weeksDelinquentInLastYear;

	public PaymentHistory(int weeksDelinquentInLastYear) {
		_weeksDelinquentInLastYear = weeksDelinquentInLastYear;
	}

	int getWeeksDelinquentInLastYear() {
		return _weeksDelinquentInLastYear;
	}
}
