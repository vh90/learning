package refactoring.introduceNullObject;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SiteTest {

	private Site _site;
	private Site _nullSite;

	@Before
	public void setUp() throws Exception {
		_site = new Site(new Customer("pippo", new BillingPlan("xxx"), new PaymentHistory(42)));
		_nullSite = new Site(new NullCustomer());
	}

	@Test
	public void plan() {
		assertThat(_site.getPlan(), is(new BillingPlan("xxx")));
		assertThat(_nullSite.getPlan(), is(new BillingPlan("default")));
	}

	@Test
	public void customerName() throws Exception {
		assertThat(_site.getCustomerName(), is("pippo"));
		assertThat(_nullSite.getCustomerName(), is("occupant"));
	}

	@Test
	public void weeksDelinquent() throws Exception {
		assertThat(_site.getWeeksDelinquent(), is(42));
		assertThat(_nullSite.getWeeksDelinquent(), is(0));
	}
}
