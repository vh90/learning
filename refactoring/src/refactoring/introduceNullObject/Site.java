package refactoring.introduceNullObject;

class Site {

	public Site(Customer customer) {
		_customer = customer;
	}

	Customer getCustomer() {
		return _customer;
	}

	private final Customer _customer;

	BillingPlan getPlan() {
		return getCustomer().getPlan();
	}

	String getCustomerName() {
		return getCustomer().getName();
	}

	int getWeeksDelinquent() {
		return getCustomer().getHistory().getWeeksDelinquentInLastYear();
	}
}
