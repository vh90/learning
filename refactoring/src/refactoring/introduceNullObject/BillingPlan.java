package refactoring.introduceNullObject;

public class BillingPlan {

	private final String _plan;

	public BillingPlan(String plan) {
		_plan = plan;
	}

	public static BillingPlan basic() {
		return new BillingPlan("default");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_plan == null) ? 0 : _plan.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillingPlan other = (BillingPlan) obj;
		if (_plan == null) {
			if (other._plan != null)
				return false;
		} else if (!_plan.equals(other._plan))
			return false;
		return true;
	}
}
