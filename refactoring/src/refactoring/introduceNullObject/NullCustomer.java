package refactoring.introduceNullObject;

class NullCustomer extends Customer {

	NullCustomer() {
		super("occupant", BillingPlan.basic(), new PaymentHistory(0));
	}

}
