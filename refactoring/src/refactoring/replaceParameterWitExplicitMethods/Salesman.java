package refactoring.replaceParameterWitExplicitMethods;

class Salesman extends Employee {

	@Override
	public int getType() {
		return SALESMAN;
	}
}
