package refactoring.replaceParameterWitExplicitMethods;

class Engineer extends Employee {

	@Override
	public int getType() {
		return ENGINEER;
	}
}
