package refactoring.replaceParameterWitExplicitMethods;

abstract class Employee {

	static final int ENGINEER = 0;
	static final int SALESMAN = 1;
	static final int MANAGER = 2;

	static Employee create(int type) {
		switch (type) {
			case ENGINEER:
				return createEngineer();
			case SALESMAN:
				return createSalesman();
			case MANAGER:
				return createManager();
			default:
				throw new RuntimeException();
		}
	}

	abstract int getType();

	static Employee createEngineer() {
		return new Engineer();
	}

	static Employee createSalesman() {
		return new Salesman();
	}

	static Employee createManager() {
		return new Manager();
	}
}
