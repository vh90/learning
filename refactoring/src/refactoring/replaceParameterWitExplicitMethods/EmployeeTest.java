package refactoring.replaceParameterWitExplicitMethods;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class EmployeeTest {

	@Test
	public void test() {
		assertThat(Employee.createEngineer().getType(), is(Employee.ENGINEER));
		assertThat(Employee.createSalesman().getType(), is(Employee.SALESMAN));
		assertThat(Employee.createManager().getType(),  is(Employee.MANAGER));
	}

}
