package refactoring.moveMethod;

class Account {

//	@Deprecated
//	double overdraftCharge() {
//		return _type.overdraftCharge(_daysOverdrawn);
//	}

	double bankCharge() {
		double result = 4.5;
		if (_daysOverdrawn > 0)
			result += _type.overdraftCharge(_daysOverdrawn);
		return result;
	}

	private AccountType _type;
	private int _daysOverdrawn;
}
