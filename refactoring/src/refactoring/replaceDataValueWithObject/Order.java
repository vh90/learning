package refactoring.replaceDataValueWithObject;

class Order {

	private Customer _customer;

	//Further refactoring may well cause me to add a new constructor and setter that takes an existing
	//customer.
	public Order (String customerName) {
		_customer = new Customer(customerName);
	}

	public String getCustomerName() {
		return _customer.getName();
	}

	public void setCustomer(String customerName) {
		_customer = new Customer(customerName);
	}
}
