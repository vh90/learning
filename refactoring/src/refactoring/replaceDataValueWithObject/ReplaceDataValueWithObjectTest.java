package refactoring.replaceDataValueWithObject;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;

public class ReplaceDataValueWithObjectTest {

	@Test
	public void test() {
		Collection<Order> orders = Arrays.asList(
				new Order("Pippo"),
				new Order("Pluto"),
				new Order("Pippo")
			);
		assertThat(numberOfOrdersFor(orders, "Pippo"), is(2));
	}

	private static int numberOfOrdersFor(Collection<Order> orders, String customer) {
		int result = 0;
		for (Order order : orders) {
			if (order.getCustomerName().equals(customer)) result++;
		}
		return result;
	}
}
