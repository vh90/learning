package refactoring.pushDownFieldMethod;

class Salesman extends Employee {

//	private final double _sales;
//
//	Salesman(double baseSalary, double quota, int sales) {
//		super(baseSalary, quota);
//		_sales = sales;
//	}
//
//	@Override
//	double salary() {
//		return _baseSalary + _sales * getQuota();
//	}

	private final double _sales;
	private final double _quota;
	
	Salesman(double baseSalary, double quota, int sales) {
		super(baseSalary);
		_quota = quota;
		_sales = sales;
	}

	double getQuota() {
		return _quota;
	}

	@Override
	double salary() {
		return _baseSalary + _sales * getQuota();
	}

}
