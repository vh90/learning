package refactoring.pushDownFieldMethod;

abstract class Employee {

//	protected double _quota;
//	protected double _baseSalary;
//
//	Employee(double baseSalary, double quota) {
//		_quota = quota;
//		_baseSalary = baseSalary;
//	}
//
//	double getQuota() {
//		return _quota;
//	}
//
//	abstract double salary();
	
	protected double _baseSalary;
	
	Employee(double baseSalary) {
		_baseSalary = baseSalary;
	}
	
	abstract double salary();
}
