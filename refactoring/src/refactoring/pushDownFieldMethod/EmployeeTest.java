package refactoring.pushDownFieldMethod;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class EmployeeTest {

	@Test
	public void test() {
		assertThat(new Salesman(3000, 0.05, 100).salary(), is(3005.00));
		assertThat(new Engineer(4000).salary(), is(4000.00));
	}

}
