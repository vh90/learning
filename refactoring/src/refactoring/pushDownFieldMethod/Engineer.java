package refactoring.pushDownFieldMethod;

class Engineer extends Employee {

//	Engineer(double baseSalary) {
//		super(baseSalary, 0);
//	}
//
//	@Override
//	double salary() {
//		return _baseSalary;
//	}
	
	Engineer(double baseSalary) {
		super(baseSalary);
	}
	
	@Override
	double salary() {
		return _baseSalary;
	}

}
